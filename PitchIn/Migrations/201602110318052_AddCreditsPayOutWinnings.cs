namespace PitchIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreditsPayOutWinnings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "credits", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "totalPayout", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.AspNetUsers", "totalWinnings", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "totalWinnings");
            DropColumn("dbo.AspNetUsers", "totalPayout");
            DropColumn("dbo.AspNetUsers", "credits");
        }
    }
}
