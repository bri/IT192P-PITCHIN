namespace PitchIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Earnings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Earnings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobID = c.Int(nullable: false),
                        Date_Created = c.DateTime(nullable: false),
                        Amount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Jobs", "isBanned", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "isBanned");
            DropTable("dbo.Earnings");
        }
    }
}
