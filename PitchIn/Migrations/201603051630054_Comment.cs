namespace PitchIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Comment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "commentDetails", c => c.String());
            DropColumn("dbo.Comments", "Details");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "Details", c => c.String());
            DropColumn("dbo.Comments", "commentDetails");
        }
    }
}
