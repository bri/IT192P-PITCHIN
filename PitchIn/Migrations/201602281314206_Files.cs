namespace PitchIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Files : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        FileId = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 100),
                        Content = c.Binary(),
                        FileType = c.Int(nullable: false),
                        PersonId = c.Int(nullable: false),
                        Person_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.FileId)
                .ForeignKey("dbo.AspNetUsers", t => t.Person_Id)
                .Index(t => t.Person_Id);
            
            DropColumn("dbo.WorkerEntries", "FileName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WorkerEntries", "FileName", c => c.String());
            DropForeignKey("dbo.Files", "Person_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Files", new[] { "Person_Id" });
            DropTable("dbo.Files");
        }
    }
}
