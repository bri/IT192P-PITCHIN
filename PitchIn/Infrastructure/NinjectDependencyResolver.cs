﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using PitchIn.Domain.Entities;
using PitchIn.Domain.Abstract;
using PitchIn.Domain.Concrete;
using Moq;



namespace PitchIn.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
      
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            Mock<IJobRepository> mock = new Mock<IJobRepository>();

            mock.Setup(m => m.Jobs).Returns(new List<Job>
            {
                new Job {Name="a",Details="details"  },
                new Job {Name="b",Details="details" }

            });


            //            kernel.Bind<IJobRepository>().ToConstant(mock.Object);
            kernel.Bind<IJobRepository>().To<EFJobRepository>();
        }
    }
}