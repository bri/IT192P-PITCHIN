﻿using PitchIn.Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace PitchIn.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public ActionResult Index(int id)
        {
            using(var db = new ApplicationDbContext())
            {
            //    var id = User.Identity.GetUserId();
                var file = db.Files.Find(id);
                return File(file.Content, file.ContentType);
            }
            
        }
    }
}