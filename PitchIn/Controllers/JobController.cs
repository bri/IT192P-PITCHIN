﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PitchIn.Domain.Abstract;
using PitchIn.Domain.Entities;
using PitchIn.Domain.Concrete;
using PitchIn.Models;
using System.Drawing;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace PitchIn.Controllers
{
    public class JobController : Controller
    {
        int jobID = 1;
        private IJobRepository repository;

        public int pageSize = 5;

        public JobController(IJobRepository repo)
        {
            repository = repo;
        }


        public ActionResult Close(int jobID)
        {
            var db = new ApplicationDbContext();
            var updateClose = db.Jobs.Where(x => x.Job_ID == jobID).First();
            updateClose.isOpen = false;
            db.SaveChanges();
            TempData["Msg"] = "Challenge has been closed succeessfully";
            return RedirectToAction("CloseChallenge", "Job");
        }
        // GET: Job/Details/5

        //[HttpGet]
        //public ActionResult EditComment(string id)
        //{
        //    var viewComment = new TimelineViewModel();
        //    viewComment.comment.Id = Convert.ToInt32(id);
        //    return View(viewComment);

        //}   

        //[HttpPost]
        //public ActionResult Details(TimelineViewModel Jobcomment)
        //{
        //    if(ModelState.IsValid)
        //    {
        //        //var db = new DefaultConnection;
        //        //Comments comment = db.Comments.Find(id);
        //        //comment.commentDetails = Jobcomment.comment.commentDetails;
        //        try
        //        {
        //            repository.editComment(Jobcomment.comment);
        //            return RedirectToAction("Accepted", "Worker");
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }
        //    return View(Jobcomment.comment);
        //}

        public ActionResult CloseChallenge()
        {

            var db = new ApplicationDbContext();

            var userId = User.Identity.GetUserId();
            var userJobs = db.Jobs.Where(j => j.UserId == userId);
         
            var model = new TimelineViewModel();
            model.Jobs = userJobs;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;

            model.User = _user;



            return View(model);

        }


        public ActionResult OpenJob(int jobID)
        {
            var db = new ApplicationDbContext();
            var updateClose = db.Jobs.Where(x => x.Job_ID == jobID).First();
            updateClose.isOpen = true;
            db.SaveChanges();
            TempData["Msg"] = "Challenge has been opened succeessfully";
            return RedirectToAction("CloseChallenge", "Job");
        }

        public ActionResult DeleteJob(int jobID)
        {
            var db = new ApplicationDbContext();
            Job job = db.Jobs.Find(jobID);
            db.Jobs.Remove(job);
            db.SaveChanges();
            TempData["Msg"] = "Challenge has been deleted succeessfully";
            return RedirectToAction("Details", "Job", new { jobID = jobID }); 
        }

        public ActionResult Delete(int commentID)
        {
            var db = new ApplicationDbContext();

            Comments comment = db.Comments.Find(commentID);
            //var _jobid = comment.JobID;
            db.Comments.Remove(comment);
            db.SaveChanges();
            TempData["Msg"] = "Data has been deleted succeessfully";
            //     return RedirectToAction("Accepted", "Worker");
            //   return RedirectToAction("Details", "Job", new { jobID = _jobid });
            return View();
            //return Json( new { deleteCmt = comment}, JsonRequestBehavior.AllowGet );
        }

        public ActionResult DeletePhoto(int photoID)
        {
            var db = new ApplicationDbContext();

            Photo photo = db.Photos.Find(photoID);
            var _submitID = photo.WorkerEntry_Id;
            db.Photos.Remove(photo);
            db.SaveChanges();
            return RedirectToAction("Submit", "Job", new { id = _submitID });
        }


        [HttpGet]
        public ActionResult Details(int jobID)
        {
            ViewBag.jobID = jobID.ToString();
           
            string userID = User.Identity.GetUserId();
            var db = new ApplicationDbContext();
            var detailJob = repository.Jobs.Where(x => x.Job_ID == jobID).SingleOrDefault();
            var commentJob = repository.Comments.Where(x => x.JobId == jobID.ToString()).ToList();
          
            var getAllSubmitted = repository.AcceptWorkers.Where(x => x.JobId == jobID).ToList();
         
            var getWorkerSubmitted = (from acceptWorker in db.AcceptWorkers
                                      where acceptWorker.JobId == jobID
                                      && acceptWorker.UserId == userID
                                      select acceptWorker).ToList();

          

            //for displaying the name of the workers
            var ListofNames = new Dictionary<string, string>();
            var um = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            foreach (var item in getAllSubmitted)
            {
                ListofNames.Add(item.UserId, um.FindById(item.UserId).UserName);
            }
            ViewBag.WorkerNames = ListofNames;
            TempData["workerList"] = ListofNames;


            var getClientInfo = (from Jobs in db.Jobs
                                 join userWin in db.Users
                                 on  Jobs.UserId equals userWin.Id
                                 where Jobs.Job_ID == jobID
                                 select userWin).SingleOrDefault();

            



            //-------
            #region Display the winners

            if (detailJob.WinnerFirst != null)
            {
                ViewBag.First = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(detailJob.WinnerFirst).UserName;  
            }
            if (detailJob.WinnerSecond != null)
            {
                ViewBag.Second = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(detailJob.WinnerSecond).UserName;
            }

            if (detailJob.WinnerThird != null)
            {
                ViewBag.Third = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(detailJob.WinnerThird).UserName;
            }



            #endregion
            var workerList = repository.AcceptWorkers.Where(x => x.JobId == jobID).ToList();
            #region Delete the winners in the items to prevent duplicate

            if (detailJob.WinnerFirst != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == detailJob.WinnerFirst);
                workerList.Remove(itemToRemove);
            }
            if (detailJob.WinnerSecond != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == detailJob.WinnerSecond);
                workerList.Remove(itemToRemove);
            }
            if (detailJob.WinnerThird != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == detailJob.WinnerThird);
                workerList.Remove(itemToRemove);
            }

            if (workerList.Count == 0)
            {
                ViewBag.WorkerList = null;
                TempData["workerList"] = null;
            }
               
            #endregion


            var details = new TimelineViewModel();
            details.Job = detailJob;

            //if (commentJob.Count != 0)
            //{
            //    details.Comments = commentJob;
            //}
            //else
            //{
            //    TempData["commentNone"] = "No comments yet!";


            //}

            // if (getAllSubmitted.Count > 1 && User.IsInRole("client"))
            if (User.IsInRole("client"))
            {

                details.AcceptWorker = getAllSubmitted;
                details.AppUser = getClientInfo;
                return View(details);
            }
            else if (getWorkerSubmitted.Count >= 1 && User.IsInRole("worker"))
            {
                details.AcceptWorker = getWorkerSubmitted;
                details.AppUser = getClientInfo;
                return View(details);
            }
            else
            {
                details.AppUser = getClientInfo;
                TempData["noSubmitted"] = "No submitted challenge yet!";
                return View(details);
            }

            //brian's code



        }

        //[HttpPost]
        //public ActionResult EditComment(TimelineViewModel Jobcomment)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var db = new ApplicationDbContext();
        //        //Comments comment = db.Comments.Find(id);
        //        //comment.commentDetails = Jobcomment.comment.commentDetails;
        //        try
        //        {
        //            repository.editComment(Jobcomment.comment);
        //            return RedirectToAction("List", "Client");
        //           // return RedirectToAction("Accepted", "Worker");
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }
        //    return View(Jobcomment.comment);
        //}


        [HttpPost]
        public ActionResult Details(TimelineViewModel JobComment, string jobID)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    //JobComment.comment.JobID = jobID;
                    repository.SaveComment(JobComment.comment, User.Identity.GetUserId());
                }

                //        return RedirectToAction("Accepted", "Worker");
                return RedirectToAction("Details", "Job", new { jobID = jobID });


              
            }
            catch
            {
                return View();
            }
        }

        
        // GET: Job/Create
        public ActionResult Create()
        {
            if(User.IsInRole("worker"))
            {
                return RedirectToAction("Index", "Worker");
            }

            if (User.IsInRole("client"))
            {
                var um = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                if (um.FindById(User.Identity.GetUserId()).credits >= 900)
                {
                    return View();
                }
                else
                {
                    TempData["clientCredits"] = "Insufficient credits to post a job";

                    return RedirectToAction("Index", "Client");
                }
            }
            else
                return RedirectToAction("Index", "Client");
          
        
        }

        public ActionResult SearchJob(string q)
        {
            var job = GetJobs(q);
            return PartialView(job);
        }
        private List<Job> GetJobs(string query)
        {
            return repository.Jobs.Where(j => j.Name.StartsWith(query, StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public ActionResult AutoComp(string term)
        {
            var artists = GetJobs(term).Select(a => new { value = a.Name });
            return Json(artists, JsonRequestBehavior.AllowGet);
        }



        // POST: Job/Create
        [HttpPost]
        public ActionResult Create(Job collection)
        {
            try
            {

                if (ModelState.IsValid)
                {
                  
                    repository.SaveJob(collection, User.Identity.GetUserId());
                    //var newJob = new Job
                    //{
                    //    Name = collection.Name,
                    //    Details = collection.Details,
                    //    Date_Created = DateTime.Now,
                    //    Date_End = collection.Date_End,
                    //    ApplicationId = User.Identity.GetUserId(),
                    //};

                    //var workerTemp = new AcceptWorker
                    //{
                    //    IsSubmitted = false,
                    //    Job = newJob
                    //};

                    //newJob.AcceptWorkers.Add(workerTemp);
                    //db.AcceptWorkers.Add(workerTemp);

                    //db.SaveChanges();
                    var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                    var currentUser = manager.FindById(User.Identity.GetUserId());
                    //deduct the credits
                    var totalPrizes = collection.PrizeFirst + collection.PrizeSecond + collection.PrizeThird; //total prizes
                    var postingFee = 150;
                    currentUser.credits -= totalPrizes + postingFee;
                    repository.PI_AddEarnings(collection.Job_ID, postingFee);
                    manager.Update(currentUser);

                    return RedirectToAction("List", "Client");

                }
                //TODO: not sure kung ggawa pa ng index
                //   return RedirectToAction("Index", "Client");
                return View();
            }
            catch
            {
                return View();
            }
        }

        //public ViewResult List(string v)
        //{

        //}



        // GET: Job/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Job/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Job/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Job/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}


        public static void CheckChallengeDeadline()
        {

        }


        public ActionResult GetLastFiveJobs()
        {
            var recentJobs = repository.RecenttFiveJobsCreated();

            int x = 5;

            return View(recentJobs);
        }

        public ViewResult List(int page = 1)
        {
            //wala pa ung filter para sa open challenges. testing lng muna

            //   return View(repository.Jobs.OrderBy(j => j.Job_ID).Skip((page - 1) * pageSize).Take(pageSize));

            JobListViewModel model = new JobListViewModel
            {
                Jobs = repository.Jobs.OrderBy(j => j.Job_ID).Skip((page - 1) * pageSize).Take(pageSize),
                PageInfo = new JobPaging
                {
                    Current = page,
                    ItemPerPage = pageSize,
                    Total = repository.Jobs.Count()
                }
            };
            return View(model);
        }


        public ActionResult Submit(int id, string filter = null, int page = 1, int pageSize = 20)
        {
            var _userId = User.Identity.GetUserId();
            var records = new PagedList<Photo>();
            ViewBag.filter = filter;



            var model = new TimelineViewModel();


            var db = new ApplicationDbContext();

            //records.Content = db.Photos
            //            .Where(x => filter == null ||  (x.Description.Contains(filter) && x.WorkerEntry_Id ==id && x.AcceptWorker.UserId== _userId))
            //            .OrderByDescending(x => x.PhotoId)
            //            .Skip((page - 1) * pageSize)
            //            .Take(pageSize)
            //            .ToList();

            records.Content = db.Photos.Where(x => x.WorkerEntry_Id == id).ToList();


            // Count
            records.TotalRecords = db.Photos
                            .Where(x => filter == null || (x.Description.Contains(filter))).Count();

            records.CurrentPage = page;
            records.PageSize = pageSize;

            var userId = User.Identity.GetUserId();
            var userJobs = db.Jobs.Where(j => j.UserId == userId);

            model.Jobs = userJobs;

            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;
            model.User = _user;






            model.Pagelist = records;
            return View(model);
            //    return View(records);

        }

        [HttpGet]

        public ActionResult ImageCreate(int id)
        {

            ViewBag.JobID = id;


            var db = new ApplicationDbContext();


            var photo = new Photo();





            var userId = User.Identity.GetUserId();
            var userJobs = db.Jobs.Where(j => j.UserId == userId);

            var model = new TimelineViewModel();
            model.Jobs = userJobs;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;
            model.User = _user;

            model.Photos = photo;

            // return View(photo);
            return View(model);
        }

        public Size NewImageSize(Size imageSize, Size newSize)
        {
            Size finalSize;
            double tempval;
            if (imageSize.Height > newSize.Height || imageSize.Width > newSize.Width)
            {
                if (imageSize.Height > imageSize.Width)
                    tempval = newSize.Height / (imageSize.Height * 1.0);
                else
                    tempval = newSize.Width / (imageSize.Width * 1.0);

                finalSize = new Size((int)(tempval * imageSize.Width), (int)(tempval * imageSize.Height));
            }
            else
                finalSize = imageSize; // image is already small size

            return finalSize;
        }

        private void SaveToFolder(Image img, string fileName, string extension, Size newSize, string pathToSave)
        {
            // Get new resolution
            Size imgSize = NewImageSize(img.Size, newSize);
            using (System.Drawing.Image newImg = new Bitmap(img, imgSize.Width, imgSize.Height))
            {
                newImg.Save(Server.MapPath(pathToSave), img.RawFormat);
            }
        }

        [HttpPost]
        public ActionResult ImageCreate(TimelineViewModel photo, int id, IEnumerable<HttpPostedFileBase> files)
        {
            var _userId = User.Identity.GetUserId();

            if (!ModelState.IsValid)
                return View(photo);
            if (files.Count() == 0 || files.FirstOrDefault() == null)
            {
                ViewBag.error = "Please choose a file";
                return View(photo);
            }

            var model = new Photo();
            //    model = User.Identity.GetUserId();
            var workerPool = repository.AcceptWorkers.Where(m => m.JobId == id && m.UserId == _userId).FirstOrDefault();
            workerPool.IsSubmitted = true;
            model.AcceptWorker = workerPool;

            foreach (var file in files)
            {
                if (file.ContentLength == 0) continue;

                model.Description = photo.Photos.Description;
                var fileName = Guid.NewGuid().ToString();
                var extension = System.IO.Path.GetExtension(file.FileName).ToLower();

                using (var img = System.Drawing.Image.FromStream(file.InputStream))
                {
                    model.ThumbPath = String.Format("/Gallery/thumbs/{0}{1}", fileName, extension);
                    model.ImagePath = String.Format("/Gallery/{0}{1}", fileName, extension);

                    // Save thumbnail size image, 100 x 100
                    SaveToFolder(img, fileName, extension, new Size(250, 250), model.ThumbPath);

                    // Save large size image, 800 x 800
                    SaveToFolder(img, fileName, extension, new Size(600, 600), model.ImagePath);
                }

                // Save record to database
                //using(var db = new ApplicationDbContext())
                //  {
                //      model.CreatedOn = DateTime.Now;
                //      db.Photos.Add(model);
                //      db.SaveChanges();
                //  }

                model.CreatedOn = DateTime.Now;

                repository.SavePhoto(model, _userId);
            }




            return RedirectPermanent("/home");
        }

        [HttpGet]
        public ActionResult Choose(TimelineViewModel model, int jobId)
        {
            TimelineViewModel tmv = new TimelineViewModel();
            List<string> ListofNames = new List<string>();
            var um = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var theJob = repository.Jobs.Where(x => x.Job_ID == jobId).FirstOrDefault();

            var workerList = repository.AcceptWorkers.Where(x => x.JobId == jobId).ToList();
            //get the names and save it to ListofNames
            //foreach (var item in workerList)
            //{
            //    ListofNames.Add(um.FindById(item.UserId).UserName);
            //}

            //    var dropDown = new SelectList(workerList.ToList(), "")
            #region Delete the winners in the items to prevent duplicate

            if (theJob.WinnerFirst != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == theJob.WinnerFirst);
                workerList.Remove(itemToRemove);
            }
            if (theJob.WinnerSecond != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == theJob.WinnerSecond);
                workerList.Remove(itemToRemove);
            }
            if (theJob.WinnerThird != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == theJob.WinnerThird);
                workerList.Remove(itemToRemove);
            }



            #endregion
            // pag 0 ang workerList, return sa details wala pde maging winner
            if (workerList.Count == 0)
            {
               return  RedirectToAction("Details", new { jobID = jobId });
            }


            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var item in workerList) 
            {
                items.Add(new SelectListItem { Text = um.FindById(item.UserId).UserName, Value = item.UserId });
            }
      
            ViewBag.DropDown = items;
            //-----------------------------
            //code under wala lang for testing
            List<SelectListItem> item2 = new List<SelectListItem>();
            item2.Add(new SelectListItem { Text = "123", Value = "123"});
        item2.Add(new SelectListItem { Text = "211", Value = "3123"});
            ViewBag.dd = item2;
            

            List<string> ListItems = new List<string>();
            ListItems.Add("Select");
            ListItems.Add("India");
            ListItems.Add("Australia");
            ListItems.Add("America");
            ListItems.Add("South Africa");
            SelectList Countries = new SelectList(ListItems);
            ViewData["Countries"] = Countries;
            //----------------------------------

            //check the winners in the job
          
            if (theJob.WinnerFirst == null)
            {
                ViewBag.WinnerText = "Choose your first winner";
            }
            else if(theJob.WinnerSecond==null)
            {
                ViewBag.WinnerText = "Choose your second winner";
            }
            else //for 3rd winner
            {
                ViewBag.WinnerText = "Choose your third winner";
            }

            return View();
        }

        [HttpPost]
        public ActionResult Choose(TimelineViewModel model, int jobId, FormCollection form,string Message)
        {
           string strDDLValue = form["DropDown"].ToString();
            if(Message == "Choose your first winner")
            {
                repository.FirstWinner(jobId, strDDLValue);
            }
            if (Message == "Choose your second winner")
            {
                repository.SecondWinner(jobId, strDDLValue);
            }
            if (Message == "Choose your third winner")
            {
                repository.ThirdWinner(jobId, strDDLValue);
            }
            ViewBag.Message ="You selected " +HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(strDDLValue).UserName;
            TempData["PrevWinner"] = ViewBag.Message;
            //----------------
            //populate again the dropdownlist
            var um = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var workerList = repository.AcceptWorkers.Where(x => x.JobId == jobId).ToList();
          
            //filtering the items in dropdownlist
            var theJob = repository.Jobs.Where(x => x.Job_ID == jobId).FirstOrDefault();  //get the instance of the job first

            #region Delete the winners in the items to prevent duplicate

            if (theJob.WinnerFirst != null)
            {
             var itemToRemove = workerList.Find(x => x.UserId == theJob.WinnerFirst);
                workerList.Remove(itemToRemove);
            }
            if (theJob.WinnerSecond != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == theJob.WinnerSecond);
                workerList.Remove(itemToRemove);
            }
            if (theJob.WinnerThird != null)
            {
                var itemToRemove = workerList.Find(x => x.UserId == theJob.WinnerThird);
                workerList.Remove(itemToRemove);
            }



            #endregion
            // pag 0 ang workerList, return sa details wala pde maging winner
            if (workerList.Count == 0)
            {
                return RedirectToAction("Details", new { jobID = jobId });
            }


            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var item in workerList)
            {
                items.Add(new SelectListItem { Text = um.FindById(item.UserId).UserName, Value = item.UserId });
            }
            ViewBag.DropDown = items;



            return View();
        }


        public JsonResult States(string Country)
        {
            List<string> StatesList = new List<string>();
            switch (Country)
            {
                case "India":
                    StatesList.Add("New Delhi");
                    StatesList.Add("Mumbai");
                    StatesList.Add("Kolkata");
                    StatesList.Add("Chennai");
                    break;
                case "Australia":
                    StatesList.Add("Canberra");
                    StatesList.Add("Melbourne");
                    StatesList.Add("Perth");
                    StatesList.Add("Sydney");
                    break;
                case "America":
                    StatesList.Add("California");
                    StatesList.Add("Florida");
                    StatesList.Add("New York");
                    StatesList.Add("Washignton");
                    break;
                case "South Africa":
                    StatesList.Add("Cape Town");
                    StatesList.Add("Centurion");
                    StatesList.Add("Durban");
                    StatesList.Add("Jahannesburg");
                    break;
            }
            return Json(StatesList);


        }


        //public PartialViewResult GetCommentData(TimelineViewModel model, string jobID)
        //{
        //    var newComment = new Comments();
        //  model.comment.JobID = jobID;
        //   var  userID = User.Identity.GetUserId();
        //   repository.SaveComment(model.comment,userID);
        //    newComment = repository.Comments.Where(x => x.JobID == jobID && x.UserId == userID).LastOrDefault();
        //    //newComment.commentDetails = model.comment.commentDetails;
        //    //newComment.UserId = User.Identity.GetUserId();

          
        //    return PartialView(newComment);
        //}

        //public JsonResult GetCommentDataJson(TimelineViewModel model)
        //{
         
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}


    }
}
