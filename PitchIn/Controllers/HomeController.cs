﻿using PitchIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PitchIn.Domain.Concrete;

namespace PitchIn.Controllers
{
    public class HomeController : Controller
    {
        #region testing
        private Domain.Abstract.IJobRepository repository;

        public HomeController(Domain.Abstract.IJobRepository repo)
        {
            repository = repo;
        }


        public string Test()
        {
            repository.CreateTestTable();

            return "Success";
        }
        #endregion




        public async Task<ActionResult> Index()
        {
            var roles = ApplicationRoleManager.Create(HttpContext.GetOwinContext());

            if (!await roles.RoleExistsAsync(SecurityRoles.Client))
            {
                await roles.CreateAsync(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = SecurityRoles.Client });
            }
            if (!await roles.RoleExistsAsync(SecurityRoles.Worker))
            {
                await roles.CreateAsync(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = SecurityRoles.Worker });
            }

            if (User.IsInRole("client"))
                return RedirectToAction("Index", "Client");

            if (User.IsInRole(SecurityRoles.Worker))
            {
                return RedirectToAction("Index", "Worker");
            }


            return View();

        }




        //public ActionResult Index()
        //{


        //    if (User.IsInRole("client"))
        //        return RedirectToAction("Index", "Client");

        //    if (User.IsInRole(SecurityRoles.Worker))
        //    {
        //        return RedirectToAction("Index", "Worker");
        //    }

        //    return View();
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}