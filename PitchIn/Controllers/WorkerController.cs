﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PitchIn.Models;
using PitchIn.Domain.Concrete;
using PitchIn.Domain.Entities;
using PitchIn.Domain.Abstract;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace PitchIn.Controllers
{
    [Authorize(Roles = "worker")]
    public class WorkerController : Controller
    {
      
        private IJobRepository repository;

        public  WorkerController(IJobRepository repo)
        {
            repository = repo;
        }
        [Authorize]
        public ActionResult SubmitEntry(int id)
        {


            return View();
        }
        [HttpPost]
        public ActionResult SubmitEntry(int id, HttpPostedFileBase file)
        {
         

            //var workerTemp = new AcceptWorker
            //{
            //    IsSubmitted = false,
            //    Job = job
            //};

            //job.AcceptWorkers.Add(workerTemp);


            if (file != null)
            {
                var userId = User.Identity.GetUserId();
                repository.SubmitEntry(id, userId, file);

             return   RedirectToAction("Index", "Home");
                //var job = repository.Jobs.Where(j => j.Job_ID == id).FirstOrDefault();
                //if (job != null)
                //{

                //    using (var db = new ApplicationDbContext())
                //    {
                //        db.AcceptWorkers.Add(new AcceptWorker { UserId = User.Identity.GetUserId(), Job_ID = id });
                //        db.SaveChanges();
                //    }
            

                }
                return View();
        }


        public ActionResult Remove(int jobID)
        {
            var db = new ApplicationDbContext();
            var updateRemove = db.AcceptWorkers.Where(x => x.JobId == jobID).First(); 
            db.AcceptWorkers.Remove(updateRemove);
            db.SaveChanges();
            TempData["Msg"] = "Succesfully removed the work accepted.";
            return RedirectToAction("Accepted", "Job");
        }

        // GET: Worker\
        [HttpGet]
        public ActionResult Index()
        {
            var db = new ApplicationDbContext();
            string _userId = User.Identity.GetUserId();
         
            //var getRecent = 

            TimelineViewModel tmv = new TimelineViewModel();

            //var getRecentSubmission = (from getRecent in repository.AcceptWorkers
            //                           join getRecentPhoto in repository.Photos
            //                           on getRecent.Id equals getRecentPhoto.WorkerEntry_Id
            //                           where getRecent.UserId == _userId
            //                           orderby getRecentPhoto.CreatedOn descending
            //                           select getRecent).FirstOrDefault();

            //var acceptWorkerList = db.AcceptWorkers.Where(x => x.UserId == _userId).FirstOrDefault();
            //var acceptJobList = db.Jobs.Where(j => j.AcceptWorkers.Any(a => a.UserId == _userId)).FirstOrDefault();

            var getRecentSubmission = (from getRecent in db.AcceptWorkers
                                       join getRecentPhoto in db.Photos
                                       on getRecent.Id equals getRecentPhoto.WorkerEntry_Id
                                       where getRecent.UserId == _userId
                                       orderby getRecentPhoto.CreatedOn descending
                                       select getRecent).FirstOrDefault();
            var getTopWorker = (from getTop10 in db.Users
                                orderby getTop10.totalWinnings descending
                                where getTop10.totalWinnings > 0
                                select getTop10).Take(10);

            var firstPrize = (from fPrize in db.Jobs
                              orderby fPrize.PrizeFirst descending
                              where fPrize.WinnerFirst == _userId
                              select fPrize).FirstOrDefault();
            /*db.Jobs.OrderByDescending(x => x.PrizeFirst).FirstOrDefault();*/
            var secPrize = (from sPrize in db.Jobs
                            orderby sPrize.PrizeSecond descending
                            where sPrize.WinnerSecond == _userId
                            select sPrize).FirstOrDefault();

            /*db.Jobs.OrderByDescending(x => x.PrizeSecond).FirstOrDefault();*/
            var thirdPrize = (from tPrize in db.Jobs
                              orderby tPrize.PrizeThird descending
                              where tPrize.WinnerThird == _userId
                              select tPrize).FirstOrDefault();
            /* db.Jobs.OrderByDescending(x => x.PrizeThird).FirstOrDefault();*/


            //if (firstPrize.WinnerFirst != null)
            //{
            //    ViewBag.Win = firstPrize.PrizeFirst;
            //}
            //else if (secPrize.WinnerSecond != null)
            //{
            //    ViewBag.Win = secPrize.PrizeSecond;
            //}

            //else if (thirdPrize.WinnerThird != null)
            //{
            //    ViewBag.Win = thirdPrize.PrizeThird;
            //}

            //var getTopWorker = db.Users.OrderByDescending(x => x.totalWinnings).Take(10);
            tmv.getRecent = getRecentSubmission;
            tmv.getTop10Win = getTopWorker;
            tmv.Job = firstPrize;
            //tmv.AcceptJobViewModel.singleWorker = acceptWorkerList;
            //tmv.AcceptJobViewModel.Job = acceptJobList;
            return View(tmv);

            //var db = new ApplicationDbContext();


            //string _userId = User.Identity.GetUserId();

            ////var acceptList = db.AcceptWorkers.Where(m => m.UserId == _userId).ToList();

            //var acceptJobList = db.Jobs.Where(j => j.AcceptWorkers.Any(a => a.UserId == _userId)).ToList();
            ////Job asdf = new Job { Job_ID }
            ////var jobs = db.Jobs.Where(model => acceptList.Any(a => model.Job_ID.Contains(a));
            //var acceptWorkerList = db.AcceptWorkers.Where(x => x.UserId == _userId).ToList();

            //var acceptJobView = new AcceptJobViewModel();
            //acceptJobView.Jobs = acceptJobList;
            //acceptJobView.AcceptWorker = acceptWorkerList;


            //return View(acceptJobView);
            //return View();
        }

        public ActionResult WorkerDashboard()
        {
            var user = GetUser();
            ViewBag.Credits = user.credits;
            ViewBag.Earnings = user.totalWinnings;
            var db = new ApplicationDbContext();
            string _userId = User.Identity.GetUserId();

          
            TimelineViewModel tmv = new TimelineViewModel();
            

            var getRecentSubmission = (from getRecent in db.AcceptWorkers
                                       join getRecentPhoto in db.Photos
                                       on getRecent.Id equals getRecentPhoto.WorkerEntry_Id
                                       where getRecent.UserId == _userId
                                       orderby getRecentPhoto.CreatedOn descending
                                       select getRecent).FirstOrDefault();
            var getTopWorker = (from getTop10 in db.Users
                                orderby getTop10.totalWinnings descending
                                where getTop10.totalWinnings > 0
                                select getTop10).Take(10);

            var firstPrize = (from fPrize in db.Jobs
                              orderby fPrize.PrizeFirst descending
                              where fPrize.WinnerFirst == _userId
                              select fPrize).FirstOrDefault();
            var secPrize = (from sPrize in db.Jobs
                            orderby sPrize.PrizeSecond descending
                            where sPrize.WinnerSecond == _userId
                            select sPrize).FirstOrDefault();

           
            var thirdPrize = (from tPrize in db.Jobs
                              orderby tPrize.PrizeThird descending
                              where tPrize.WinnerThird == _userId
                              select tPrize).FirstOrDefault();
        


           
            tmv.getRecent = getRecentSubmission;
            tmv.getTop10Win = getTopWorker;
            tmv.Job = firstPrize;
         
            return View(tmv);
        }
        public ViewResult OpenChallenges()
        {
            #region Timelineview Model Required!
            //TimelineViewModel Required!!!
            var user = GetUser();
            Session["Credits"] = user.credits;
            ViewBag.Credits = user.credits;
            var userId = User.Identity.GetUserId();
            var userJobs = repository.Jobs.Where(j => j.UserId == userId);

            var model = new TimelineViewModel();
            model.Jobs = userJobs;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;


            model.User = _user;


            //----------------- end here
            #endregion



            ViewBag.clientCredits = userManager.FindById(User.Identity.GetUserId()).credits;
            var userID = User.Identity.GetUserId();
            //var db = new ApplicationDbContext();
            //var jobs = db.Jobs.ToList()
            //var jobs = repository.Jobs.ToList();

            var x = repository.GetOpenJobs();
            var jobList = x.ToList();
           
            HashSet<int> writerIds = new HashSet<int>(repository.AcceptWorkers.Where( s=> s.UserId== userID).Select(s => s.JobId));

            jobList.RemoveAll(s => writerIds.Contains(s.Job_ID));

            model.Jobs = jobList;


            return View(model);
            


         //  return View(repository.GetOpenJobs());
           

        }

        public ActionResult Accepted()
        {
            #region Timelineview Model Required!
            //TimelineViewModel Required!!!
            var user = GetUser();
            ViewBag.Credits = user.credits;
            var userId = User.Identity.GetUserId();
            //        var userJobs = db.Jobs.Where(j => j.ApplicationId == userId); OR gamit ng repository :)
            //var userJobs = repository.Jobs.Where(j => j.UserId == userId);

            var userJobs = repository.AcceptedJobs(12, userId);
            var model = new TimelineViewModel();
            model.Jobs = userJobs;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;


            model.User = _user;

            //----------------- end here
            #endregion


          


            var db = new ApplicationDbContext();
            string _userId = User.Identity.GetUserId();

            //var acceptList = db.AcceptWorkers.Where(m => m.UserId == _userId).ToList();

            var acceptJobList = db.Jobs.Where(j => j.AcceptWorkers.Any(a => a.UserId == _userId)).ToList();
            //Job asdf = new Job { Job_ID }
            //var jobs = db.Jobs.Where(model => acceptList.Any(a => model.Job_ID.Contains(a));
            var acceptWorkerList = db.AcceptWorkers.Where(x => x.UserId == _userId).ToList();

            var acceptJobView = new AcceptJobViewModel();
            acceptJobView.Jobs = acceptJobList;
            acceptJobView.AcceptWorker = acceptWorkerList;

            model.AcceptJobViewModel = acceptJobView;

        
            return View(model);
        }
        public ApplicationUser GetUser()
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            return manager.FindById(User.Identity.GetUserId());
        }






        public ActionResult Accept(int id )
        {

            try
            {
                if (ModelState.IsValid)
               {


                    //var job = repository.Jobs.Where(j => j.Job_ID == id).FirstOrDefault();
                    //if (job != null)
                    //{

                    //    using(var db = new ApplicationDbContext())
                    //    {
                    //        db.AcceptWorkers.Add(new AcceptWorker { UserId = User.Identity.GetUserId(), JobId=id});
                    //        db.SaveChanges();
                    //    }


                    //}
                    var _userId = User.Identity.GetUserId();
                    repository.AcceptChallenge(id, _userId);

           
                }



                return RedirectToAction("Index");

            }
            catch
            {
                return View("Index");
            }

          

        }
    }
}