﻿using Microsoft.AspNet.Identity;
using PitchIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PitchIn.Domain.Concrete;
using PitchIn.Domain.Abstract;
using Microsoft.AspNet.Identity.EntityFramework;
using PitchIn.Domain.Entities;

namespace PitchIn.Controllers
{
    [Authorize(Roles="client")]
    public class ClientController : Controller
    {

        ApplicationDbContext db = new ApplicationDbContext();
        private IJobRepository repository;
        public const int RecordsPerPage = 2;

        public ClientController(IJobRepository repo)
        {
            repository = repo;
            ViewBag.RecordsPerPage = RecordsPerPage;
        }

        // GET: Client
        public ActionResult Index()
        {
            #region Timelineview Model Required!
            //TimelineViewModel Required!!!
            var user = GetUser();
            ViewBag.Credits = user.credits;
            var userId = User.Identity.GetUserId();

            //var userJobs = db.Jobs.Where(j => j.UserId == userId).FirstOrDefault();
            //var getRecentClosedCh = (from recentClose in db.Jobs
            //                          where recentClose.UserId == userId
            //                          && recentClose.isOpen == false
            //                          select recentClose).FirstOrDefault();


            //var getTopWorker = (from getTop10 in db.Users
            //                    orderby getTop10.totalWinnings descending
            //                    where getTop10.totalWinnings > 0
            //                    select getTop10).Take(10);

            var userJobs = repository.Jobs.Where(j => j.UserId == userId).FirstOrDefault();
            var getRecentClosedCh = repository.GetRecentClosedJob(userId);
            var getTopWorker = repository.GetTopWorkers();

            var model = new TimelineViewModel();
            model.Job = userJobs;
            model.getRecentClose = getRecentClosedCh;
            model.getTop10Win = getTopWorker;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;
          

            model.User = _user;


            //----------------- end here
            #endregion


            //for tagging
            //System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //ViewBag.AllTags = serializer.Serialize(repository.Jobs.ToArray());
            //ViewBag.SelectedTags = repository.Jobs.ToList();
            //
            //inf scrolling


            return RedirectToAction("GetJobs");
//            return View(model);

            //for testing only - remove this line
            // ViewBag.Credits = 0;

          //  return View(); 
        }


        public ActionResult ClientDashboard()
        {
            var user = GetUser();
            ViewBag.Credits = user.credits;
            var userId = User.Identity.GetUserId();
            var userJobs = repository.Jobs.Where(j => j.UserId == userId).FirstOrDefault();
            var getRecentClosedCh = repository.GetRecentClosedJob(userId);
            var getTopWorker = repository.GetTopWorkers();

            var model = new TimelineViewModel();
            model.Job = userJobs;
            model.getRecentClose = getRecentClosedCh;
            model.getTop10Win = getTopWorker;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;


            model.User = _user;

            return View(model);
        }


        public ActionResult GetJobs(int? pageNum)
        {
            pageNum = pageNum ?? 0;
            ViewBag.IsEndOfRecords = false;
            if (Request.IsAjaxRequest())
            {
                var customers = GetRecordsForPage(pageNum.Value);
                ViewBag.IsEndOfRecords = (customers.Any()) && ((pageNum.Value * RecordsPerPage) >= customers.Last().Key);
                return PartialView("_JobsRow", customers);
            }
            else
            {
                LoadAllCustomersToSession();
                ViewBag.Customers = GetRecordsForPage(pageNum.Value);
                return View("Index");
            }
        }

        public Dictionary<int, Job> GetRecordsForPage(int pageNum)
        {
            Dictionary<int, Job> customers = (Session["Customers"] as Dictionary<int, Job>);

            int from = (pageNum * RecordsPerPage);
            int to = from + RecordsPerPage;

            return customers
                .Where(x => x.Key > from && x.Key <= to)
                .OrderBy(x => x.Key)
                .ToDictionary(x => x.Key, x => x.Value);
        }

        public void LoadAllCustomersToSession()
        {
           
            var jobs = repository.Jobs.ToList();
            int custIndex = 1;
            Session["Customers"] = jobs.ToDictionary(x => custIndex++, x => x);
            ViewBag.TotalNumberCustomers = jobs.Count();
        }

        public ActionResult Autocomplete(string term)
        {
            var items = new[] { "Apple", "Pear", "Banana", "Pineapple", "Peach" };

            var filteredItems = items.Where(
                item => item.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) >= 0
                );
            return Json(filteredItems, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Close(int jobID)
        {
            var db = new ApplicationDbContext();
            var updateClose = db.Jobs.Where(x => x.Job_ID == jobID).First();
            updateClose.isOpen = false;
            db.SaveChanges();
            TempData["Msg"] = "Challenge has been closed succeessfully";
            return RedirectToAction("List", "Client");
        }



        public ActionResult Charge(string stripeToken, string stripeEmail)
        {
            string apiKey = "sk_test_eezHTk5yiUzIzX8qO8Dh6i1K";
            var stripeClient = new Stripe.StripeClient(apiKey);

            dynamic response = stripeClient.CreateChargeWithToken(5000, stripeToken, "USD", stripeEmail);
            var userId = User.Identity.GetUserId();

            if (response.IsError == false && response.Paid)
            {
                //success

                //get the current user then add credits
                //var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                //var currentUser = manager.FindById(User.Identity.GetUserId());
                //currentUser.credits += 5;
                //manager.Update(currentUser);

                repository.AddCredit(userId);

                //ViewBag.PaymentComplete = "Payment Successful!";
                //     ViewData["Payment"] = "Payment Successful!";
                TempData["foo"] = "Payment Successful!";

            }
            return RedirectToAction("Index");
        }


        public ActionResult List()
        {

            var userId = User.Identity.GetUserId();
            var userJobs = db.Jobs.Where(j => j.UserId == userId);


            var model = new TimelineViewModel();
            model.Jobs = userJobs;
            //---------
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser _user = userManager.FindByNameAsync(User.Identity.Name).Result;

            model.User = _user;



            return View(model);

        }


        public ApplicationUser GetUser()
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            return manager.FindById(User.Identity.GetUserId());
        }
    }
}