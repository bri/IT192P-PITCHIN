﻿using PitchIn.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PitchIn.Models;
using PitchIn.Domain.Concrete;
using PitchIn.Domain.Entities;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace PitchIn.Controllers
{
    public class AdminController : Controller
    {
        private IJobRepository repository;
        public AdminController(IJobRepository repo)
        {
            repository = repo;

        }


        

        // GET: Admin
        public ActionResult Index()
        {
            //var ar groups = _uow.Orders.GetAll()
            //.Where(x => x.Created > baselineDate)
            //.GroupBy(x => EntityFunctions.TruncateTime(x.Created));

            #region data for number of jobs created per month

            var q = from i in repository.Earnings
                    let dt = i.Date_Created
                    group i by new { y = dt.Year, m = dt.Month} into g
                    select new
                    { count=g.Count(),
                      date=g.Key,
                      month = g.Key.m,
                    
                    };
            //var jan = q.where(a => a.month == 1).count();
            //var feb = q.where(a => a.month == 2);
            //var march = q.where(a => a.month == 3).count();
            //var april = q.where(a => a.month == 4).count();
            //var may = q.where(a => a.month == 5).count();
            //var june = q.where(a => a.month == 6).count();
            //var july = q.where(a => a.month == 7).count();
            //var aug = q.where(a => a.month == 8).count();
            //var sep = q.where(a => a.month == 9).count();
            //var oct = q.where(a => a.month == 10).count();
            //var nov = q.where(a => a.month == 11).count();
            //var dec = q.where(a => a.month == 12).count();
            ViewBag.jan = q.Where(a => a.month == 1).Count();
            ViewBag.feb = q.Where(a => a.month == 2).Count();
            ViewBag.march = q.Where(a => a.month == 3).Count();
            ViewBag.april = q.Where(a => a.month == 4).Count();
            ViewBag.may = q.Where(a => a.month == 5).Count();
            ViewBag.june = q.Where(a => a.month == 6).Count();
            ViewBag.july = q.Where(a => a.month == 7).Count();
            ViewBag.aug = q.Where(a => a.month == 8).Count();
            ViewBag.sep = q.Where(a => a.month == 9).Count();
            ViewBag.oct = q.Where(a => a.month == 10).Count();
            ViewBag.nov = q.Where(a => a.month == 11).Count();
            ViewBag.dec = q.Where(a => a.month == 12).Count();
        


            #endregion

            return View();


        }

        public Product GetChartData()
        {
            Product objproduct = new Product();
            /*Get the data from databse and prepare the chart record data in string form.*/
            objproduct.Year = "Jan,Feb,2011,2012,2013,2014";
            objproduct.Sale = "2000,1000,3000,1500,2300,500";
            objproduct.Purchase = "2100,1400,2900,2400,2300,1500";
            return objproduct;
        }

        public ActionResult PieChart()
        {
            var Inc = repository.Jobs.Where(x => x.isOpen == true).Count();
            var Comp = repository.Jobs.Where(x => x.isOpen == false).Count();


            ViewBag.Inc = Inc;
            ViewBag.Comp = Comp;


            return View();
        }

        public ActionResult ListUsers()
        {
            var db = new ApplicationDbContext();
            TimelineViewModel tmv = new TimelineViewModel();
            var getUsers = (from userL in db.Users
                           where userL.isBanned == false
                           select userL).ToList();

            tmv.AppUsers = getUsers;
            return View(tmv);
        }

        public ActionResult Ban(string id)
        {
            var db = new ApplicationDbContext();
            var updateBan = db.Users.Where(x => x.Id == id).First();
            updateBan.isBanned = true;
            db.SaveChanges();
            return RedirectToAction("ListUsers", "Admin");
        }

        public ActionResult ListBan()
        {
            var db = new ApplicationDbContext();
            TimelineViewModel tmv = new TimelineViewModel();
            var getUsers = (from userL in db.Users
                            where userL.isBanned == true
                            select userL).ToList();

            tmv.AppUsers = getUsers;
            return View(tmv);
        }

        public ActionResult Unban(string id)
        {
            var db = new ApplicationDbContext();
            var updateBan = db.Users.Where(x => x.Id == id).First();
            updateBan.isBanned = false;
            db.SaveChanges();
            return RedirectToAction("ListBan", "Admin");
        }

        public ActionResult ListProjects()
        {
            return View();
        }

        public ActionResult Chart()
        {
            return View();
        }

        public ActionResult ContentEditor()
        {
            return View();
        }
    }




    public class ProductModel
    {
        public string YearTitle { get; set; }
        public string SaleTitle { get; set; }
        public string PurchaseTitle { get; set; }
        public Product ProductData { get; set; }
    }
    public class Product
    {
        public string Year { get; set; }
        public string Purchase { get; set; }
        public string Sale { get; set; }
    }


}