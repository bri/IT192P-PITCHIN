﻿using PitchIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace PitchIn.Utils
{
    public class Extensions
    {
        public static IEnumerable<SelectListItem> GetRolesforDropDownList()
        {
            IList<SelectListItem> roles = new List<SelectListItem>{
           new SelectListItem(){Text="Client", Value=Roles.Client},
           new SelectListItem(){Text="Worker", Value= Roles.Worker}
       };

            return roles;

        }
    }
}