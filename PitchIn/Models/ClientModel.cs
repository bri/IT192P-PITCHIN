﻿using PitchIn.Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PitchIn.Domain.Entities;


namespace PitchIn.Models
{
    public class ClientModel
    {
        public PitchIn.Domain.Entities.Job Job { get; set; }
        public IEnumerable<ApplicationUser> getTop10Win { get; set; }
        public PitchIn.Domain.Entities.Job getRecentClose { get; set; }
        public ApplicationUser User { get; set; }
    }
}