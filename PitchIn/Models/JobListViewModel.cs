﻿using PitchIn.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PitchIn.Models
{
    public class JobListViewModel
    {
        public IEnumerable<Job> Jobs { get; set; }
        public JobPaging PageInfo { get; set; }
    }
}