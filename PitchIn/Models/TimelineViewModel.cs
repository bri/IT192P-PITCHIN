﻿using PitchIn.Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PitchIn.Domain.Entities;


namespace PitchIn.Models
{
    public class TimelineViewModel
    {
        public IEnumerable<ApplicationUser> AppUsers { get; set; }
        public PitchIn.Domain.Concrete.ApplicationUser AppUser { get; set; }
        public PitchIn.Domain.Entities.Job getRecentClose { get; set; }
        public IEnumerable<PitchIn.Domain.Entities.Job> Jobs { get; set; }
        public ApplicationUser User { get; set; }
        public IEnumerable<ApplicationUser> getTop10Win { get; set; }
        public PitchIn.Domain.Entities.Job Job { get; set; }
        public PitchIn.Domain.Entities.Comments comment { get; set; }
        public PitchIn.Models.IndexViewModel IndexViewModel { get; set; }

        public PitchIn.Models.SetPasswordViewModel SetPasswordViewModel { get; set; }


        public PitchIn.Models.ChangePasswordViewModel ChangePasswordViewModel { get; set; }
        //public PitchIn.Domain.Concrete.ApplicationUser User { get; set; }

        //public PitchIn.Domain.Entities.Comment CommentsModel { get; set; }

        public IEnumerable<PitchIn.Domain.Entities.Comments> Comments { get; set; }

        public PitchIn.Domain.Entities.Photo Photos { get; set; }


        public PitchIn.Domain.Entities.PagedList<PitchIn.Domain.Entities.Photo> Pagelist { get; set; }


        public PitchIn.Models.AcceptJobViewModel AcceptJobViewModel { get; set; }


        public PitchIn.Domain.Entities.AcceptWorker AcceptWorkers { get; set; }


        public IEnumerable<PitchIn.Domain.Entities.AcceptWorker> AcceptWorker { get; set; }

        public PitchIn.Domain.Entities.AcceptWorker getRecent { get; set; }
    }
}