﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PitchIn.Domain.Entities;


namespace PitchIn.Models
{
    public class AcceptJobViewModel
    {
        public List<Job> Jobs { get; set; }

        public List<AcceptWorker> AcceptWorker { get; set; }

        public Job Job { get; set; }

        public AcceptWorker singleWorker { get; set; }
     

    }
}