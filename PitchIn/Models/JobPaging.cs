﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PitchIn.Models
{
    public class JobPaging
    {
        public int Total{ get; set; }
        public int ItemPerPage { get; set; }
        public int Current { get; set; }

        public int TotalPages { get
            {
                return (int)Math.Ceiling((decimal)Total / ItemPerPage);
            }
        }

    }
}