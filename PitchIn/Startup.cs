﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Owin;
using Owin;
using PitchIn.Domain.Concrete;
using System;

[assembly: OwinStartupAttribute(typeof(PitchIn.Startup))]
namespace PitchIn
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();   
           // GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection", new SqlServerStorageOptions { QueuePollInterval = TimeSpan.FromSeconds(1) });

            //app.UseHangfireDashboard();
            //app.UseHangfireServer();
            //RecurringJob.AddOrUpdate(() => EFJobRepository.CheckDeadlines(), Cron.Minutely);
        }
    }
}
