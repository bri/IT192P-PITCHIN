﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PitchIn.Domain.Abstract;
using PitchIn.Domain.Concrete;
using PitchIn.Controllers;
using PitchIn.Domain.Entities;
using Moq;

namespace PitchIn.Tests.Controllers
{
    [TestClass]
    public class JobControllerTest
    {


        [TestMethod]
        public void JobController_Create()
        {
            //arrange
            Mock<IJobRepository> mock = new Mock<IJobRepository>();

            // Arrange 
            JobController controller = new JobController(mock.Object);
            //create a job

            //act
            var result = controller.Create();

            //assert
            Assert.IsNotNull(result);
           

        }
       // [TestMethod]
       //public void JobController_Details()
       // {

       //     Mock<IJobRepository> mock = new Mock<IJobRepository>();

       //     // Arrange 
       //     JobController controller = new JobController(mock.Object);
       //     //create a job

       //     //act
       //     var result = controller.Details();

       //     //assert
       //     Assert.IsNotNull(result);
       // }

        //[TestMethod]
        //public void JobController_Edit()
        //{
        //    Mock<IJobRepository> mock = new Mock<IJobRepository>();

        //    // Arrange 
        //    JobController controller = new JobController(mock.Object);
        //    //create a job

        //    //act
        //    var result = controller.Edit(1);

        //    //assert
        //    Assert.IsNotNull(result);

        //}



        [TestMethod]
        public void JobController_Delete()
        {

            Mock<IJobRepository> mock = new Mock<IJobRepository>();

            // Arrange 
            JobController controller = new JobController(mock.Object);
            //create a job

            //act
            var result = controller.Delete(1);

            //assert
            Assert.IsNotNull(result);


        }



    }
}
