﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PitchIn.Domain.Abstract;
using PitchIn.Domain.Entities;
using PitchIn.Domain.Concrete;
using PitchIn.Controllers;
using System.Collections.Generic;
using Moq;
using System.Web.Mvc;
using System.Linq;
using PitchIn.Models;
using PitchIn.Helpers;

namespace PitchIn.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void Can_View_OpenChallenges()
        {
            //arrange - should only have only 2 count
            Mock<IJobRepository> mock = new Mock<IJobRepository>();
            mock.Setup(m => m.Jobs).Returns(new Job[]
            {
                         new Job {Job_ID = 1, Name = "P1", ApplicationId = "a1",isOpen=true},
            new Job {Job_ID = 2, Name = "P2", ApplicationId = "a2",isOpen=true},
            new Job {Job_ID = 3, Name = "P3", ApplicationId = "a3", isOpen=false},
            new Job {Job_ID = 4, Name = "P4", ApplicationId = "a4", isOpen=false},
            new Job {Job_ID = 5, Name = "P5", ApplicationId = "a5",isOpen=false}
            });

            //act
            WorkerController controller = new WorkerController(mock.Object);




            int result = controller.OpenChallenges().ViewEngineCollection.Count;



            //assert

            Assert.AreEqual(2, result);


        }



        [TestMethod]
        public void Can_Save_Job()
        {
            //arrange
            // Arrange
            // - create the mock repository
            Mock<IJobRepository> mock = new Mock<IJobRepository>();

            // Arrange 
            JobController controller = new JobController(mock.Object);
            //create a job
            Job job = new Job();
            var result = controller.Create(job);
            mock.Verify(x => x.SaveJob(job, "asdf"));

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Job_Paginate()
        {
            // Arrange
            Mock<IJobRepository> mock = new Mock<IJobRepository>();
            mock.Setup(m => m.Jobs).Returns(new Job[] {
                        new Job {Job_ID = 1,  Name = "P1"},
                        new Job {Job_ID = 2, Name = "P2"},
                        new Job {Job_ID = 3, Name = "P3"},
                        new Job {Job_ID = 4, Name = "P4"},
                        new Job {Job_ID = 5, Name = "P5"}
                        });

            JobController contrlr = new JobController(mock.Object);
            contrlr.pageSize = 2;

            // Act
            // var result = (IEnumerable<Job>)contrlr.List(2).Model;
            JobListViewModel result = (JobListViewModel)contrlr.List(2).Model;
            
            // Assert
            Job[] jobArray = result.Jobs.ToArray();
            Assert.IsTrue(jobArray.Length == 2);
            Assert.AreEqual(jobArray[0].Name, "P3");
            Assert.AreEqual(jobArray[1].Name, "P4");


        }


        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            // Arrange - define an HTML helper - we need to do this
            // in order to apply the extension method
            HtmlHelper myHelper = null;
            // Arrange - create PagingInfo data
            JobPaging pagingInfo = new JobPaging
            {
                Current = 2,
                Total = 28,
                ItemPerPage = 10
            };
            // Arrange - set up the delegate using a lambda expression
            Func<int, string> pageUrlDelegate = i => "Page" + i;
            // Act
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);
            // Assert
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>"
            + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>"
            + @"<a class=""btn btn-default"" href=""Page3"">3</a>",
            result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_JobL_List_View_Model()
        {
            // Arrange
            Mock<IJobRepository> mock = new Mock<IJobRepository>();
            mock.Setup(m => m.Jobs).Returns(new Job[] {
                    new Job {Job_ID = 1, Name = "P1"},
                    new Job {Job_ID = 2, Name = "P2"},
                    new Job {Job_ID = 3, Name = "P3"},
                    new Job {Job_ID = 4, Name = "P4"},
                    new Job {Job_ID = 5, Name = "P5"}
                    });
            // Arrange
            JobController controller = new JobController(mock.Object);
            controller.pageSize = 3;
            // Act
            JobListViewModel result = (JobListViewModel)controller.List(2).Model;
            // Assert
            JobPaging pageInfo = result.PageInfo;
            Assert.AreEqual(pageInfo.Current, 2);
            Assert.AreEqual(pageInfo.ItemPerPage, 3);
            Assert.AreEqual(pageInfo.Total, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);
        }



        //public void Can_Add_To_Cart()
        //{
        //    //arrange 
        //    Mock<IProductRepository> mock = new Mock<IProductRepository>();
        //    mock.Setup(m => m.Products).Returns(new Product[]{
        //            new Product{ProductID=1,Name="P1",Category="Apples"},
        //        }.AsQueryable());

        //    // Arrange - create a Cart
        //    Cart cart = new Cart();
        //    // Arrange - create the controller
        //    CartController target = new CartController(mock.Object, null);
        //    // Act - add a product to the cart
        //    target.AddToCart(cart, 1, null);

        //    // Assert
        //    Assert.AreEqual(cart.Lines.Count(), 1);
        //    Assert.AreEqual(cart.Lines.ToArray()[0].Product.ProductID, 1);
        //}





    }
}