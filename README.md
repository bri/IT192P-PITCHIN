This is a "freelancing" ASP MVC 5 project.

Required Tools:
VS 2015

Note: You can also use VS 2013 but you have to change the web.config. On the DefaultConnection string, change "Data Source=(LocalDb)\MSSQLLocalDB...
to "Data Source=(LocalDb)\v11.0..."
It should work or feel free to contact me.

HOW TO USE
1. First you need the database (.mdf) file. https://drive.google.com/folderview?id=0B7y4d5uPVzmfcEJiejhWTUV5c0k&usp=sharing 

2. Save the .mdf to App_Code folder or 
it will not work because there are some database access right away when you start the program.
Yes I know I should have put validations to prevent stupid mistake like that but
I don't have time right now to fix that :)


NOTE:
I'll be updating the database so you might get some errors in the future
In the event that you feel the errors are due to the database, go to
package console manager. Then enter this command 'update-database -verbose -force' 
and close your eyes and convince yourself that everything's gonna be alright.