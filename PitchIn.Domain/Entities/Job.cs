﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using PitchIn.Domain.Entities;

namespace PitchIn.Domain.Entities
{
    [Prizes]
    public class Job
    {
       
        public virtual Concrete.ApplicationUser User { get; set; }

        public string UserId { get; set; }
        [Key]
        public int Job_ID { get; set; }

        [Required]
         [StringLength(20, MinimumLength = 3)]
        public string Name { get; set; }


        [DataType(DataType.MultilineText)]
        [Required]
        public string Details { get; set; }



        [HiddenInput(DisplayValue = false)]
        public DateTime Date_Created { get; set; }


        [Display(Name = "Challenge Deadline")]
        [DataType(DataType.DateTime)]
        [Required]

        public DateTime Date_End { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //public virtual ApplicationUser User { get; set; }

        public string WinnerFirst { get; set; }
        public string WinnerSecond { get; set; }
        public string WinnerThird { get; set; }
     
        [Required]
        [Display(Name = "First Prize")]
        public int PrizeFirst { get; set; }

     
        [Required]
        [Display(Name = "Second Prize")]
        public int PrizeSecond { get; set; }

        
        [Required]
        [Display(Name = "Third Prize")]
        public int PrizeThird { get; set; }



        public ICollection<AcceptWorker> AcceptWorkers { get; set; }
        public Job()
        {
            AcceptWorkers = new List<AcceptWorker>();
        }
        public bool isOpen { get; set; }

        public bool isBanned { get; set; }





    }
   

    public class WorkerEntry
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        public byte[] Image { get; set; }

        public string ImageMimeType { get; set; }


        public int WorkerEntry_Id { get; set; }

        [ForeignKey("WorkerEntry_Id")]
        public virtual AcceptWorker AcceptWorker { get; set; }


    }
    public class PrizesAttribute : ValidationAttribute,IClientValidatable
    {
        public PrizesAttribute()
        {
            ErrorMessage = "There is an error in your prizes";
 
        }
        public override bool IsValid(object value)
        {
            Job app = value as Job;
            if (app == null)
            {
                return true;
            }
            if(app.PrizeFirst> app.PrizeSecond)
            {
                if (app.PrizeSecond > app.PrizeThird)
                {
                    if((app.PrizeFirst + app.PrizeSecond + app.PrizeThird) >= 750)
                    {
                        return true;//base.IsValid(value);

                    }
                    else
                    {
                        ErrorMessage = "Total prizes must be greater than or equal to 750  ( The total is "+ (app.PrizeFirst+app.PrizeSecond+app.PrizeThird)+". Add more!)";
                        return false;
                    }
                }
                else
                {
                    ErrorMessage = "Second prize must be greater than third prize";
                    return false;
                }
            }
            else
            {
                ErrorMessage = "First prize must be greater than second prize";
                return false;
            }

        }
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "prizes"
            };
        }

    }
}