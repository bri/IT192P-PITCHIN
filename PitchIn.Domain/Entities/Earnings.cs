﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PitchIn.Domain.Entities
{
   public class Earnings
    {
        public int Id { get; set; }
        public int JobID { get; set; }
        public Job Job { get; set; }



        [HiddenInput(DisplayValue = false)]
        public DateTime Date_Created { get; set; }
        public double Amount { get; set; }
        
    }
    public class PI
    {
        public int Id { get; set; }
        public double totalEarnings { get; set; }

    }


}
