﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PitchIn.Domain.Entities
{
  public  class Photo
    {
        [Key]
        public int PhotoId { get; set; }

        [Display(Name = "Decription")]
        [Required]
        public String Description { get; set; }

        [Display(Name = "Image Path")]
        public String ImagePath { get; set; }

        [Display(Name = "Thumb Path")]
        public String ThumbPath { get; set; }


        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        public int WorkerEntry_Id { get; set; }

        [ForeignKey("WorkerEntry_Id")]
        public virtual AcceptWorker AcceptWorker { get; set; }

    }

    public class PagedList<T>
    {
        public List<T> Content { get; set; }
        public Int32 CurrentPage { get; set; }
        public Int32 PageSize { get; set; }
        public int TotalRecords { get; set; }
    }


}
