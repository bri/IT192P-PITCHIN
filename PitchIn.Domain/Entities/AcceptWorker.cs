﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PitchIn.Domain.Entities
{
 
    public class AcceptWorker
    {
        //public AcceptWorker()
        //{
        //    WorkerEntries = new List<WorkerEntry>();
        //}
        public int Id { get; set; }
        public string UserId { get; set; }
        public bool IsSubmitted { get; set; }

        public int JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        public bool IsViewed { get; set; }

        public ICollection<WorkerEntry> WorkerEntries { get; set; }

    }

}
