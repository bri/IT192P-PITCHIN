﻿using PitchIn.Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PitchIn.Domain.Entities
{
    public class Comments
    {
        public int Id { get; set; }
        //public string UserId { get; set; }

        public string JobId { get; set; }

        public string commentDetails { get; set; }
        public Job Job { get; set; }

        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
    }

}
