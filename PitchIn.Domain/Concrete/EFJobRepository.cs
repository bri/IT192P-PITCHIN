﻿//#define debug
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PitchIn.Domain.Abstract;
using PitchIn.Domain.Entities;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Security.Principal;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Web;

namespace PitchIn.Domain.Concrete
{
    public class EFJobRepository : IJobRepository
    {






        private ApplicationDbContext context = new ApplicationDbContext();
        //  private EFDbContext context = new EFDbContext();
        //private ApplicationDbContext context = new ApplicationDbContext();

        public static void CheckDeadlines()
        {
            var repo = new ApplicationDbContext();

            var challenges = repo.Jobs.Where(x => x.isOpen == true);

            if (challenges.Count() != 0)
            {
                foreach (var item in challenges)
                {
                    if (item.Date_End <= DateTime.Now)
                        item.isOpen = false;
                }


                repo.SaveChanges();
            }

        }

        public IEnumerable<Job> Jobs
        {
            get
            {
                return context.Jobs;
            }
        }

        public IEnumerable<AcceptWorker> AcceptWorkers
        {
            get
            {
                return context.AcceptWorkers;
            }
        }

        public IEnumerable<Comments> Comments
        {
            get
            {
                return context.Comments;
            }
        }

        public IEnumerable<WorkerEntry> WorkerEntries
        {
            get
            {
                return context.WorkerEntries;

            }
        }

        public IEnumerable<Photo> Photos
        {
            get
            {
                return context.Photos;
            }
        }

        public IEnumerable<Job> RecenttFiveJobsCreated()
        {
            return context.Jobs.OrderByDescending(x => x.Date_End).Where(x => x.isOpen == true).Take(5);

        }

        public IEnumerable<Job> GetOpenJobs()
        {
            return context.Jobs.Where(x => x.isOpen == true);

        }
        public IEnumerable<Earnings> Earnings
        {
            get
            {
                return context.Earnings;
            }
        }
        public Job DeleteJob(int jobId)
        {
            throw new NotImplementedException();
        }
        //public void SaveJob(Job job)
        //{
        //    if (job.Job_ID== 0)
        //    {
        //        context.Jobs.Add(job);
        //    }
        //    else
        //    {
        //        Job dbEntry = context.Jobs.Find(job.Job_ID);
        //        if (dbEntry != null)
        //        {
        //            dbEntry.Name =job.Name;
        //            dbEntry.Details = job.Details;
        //           //TODO: kulang pa
        //        }
        //    }
        //    context.SaveChanges();
        //}

        public void SaveJob(Job collection, string userID)
        {
#if debug
           
            var job = new Job
            {
                Name = collection.Name,
                Details = collection.Details,
                Date_Created = DateTime.Now,
                Date_End = collection.Date_End,
                ApplicationId = userID,
                PrizeThird = collection.PrizeThird,
                PrizeSecond = collection.PrizeSecond,
                PrizeFirst = collection.PrizeFirst,
                isOpen = true
            };


            context.Jobs.Add(collection);
            context.Database.Log = Console.WriteLine;


#endif

          
            var job = new Job
            {
                Name = collection.Name,
                Details = collection.Details,
                Date_Created = DateTime.Now,
                Date_End = collection.Date_End,
                //ApplicationId = userID,
                PrizeThird = collection.PrizeThird,
                PrizeSecond = collection.PrizeSecond,
                PrizeFirst = collection.PrizeFirst,
                isOpen = true
            };
          
          var  UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            ApplicationUser _user = UserManager.FindById(userID);
         

            //insert new data
            if (job.Job_ID == 0)
            {
                //   context.Jobs.Add(job);

                //  var workerTemp = new AcceptWorker
                //  {
                //      IsSubmitted = false,
                //      Job = job
                //  };

                //job.AcceptWorkers.Add(workerTemp);

                //this one is working
                #region using ef
                //context.Database.Log = x => Trace.Write(x);
                //_user.Jobs.Add(job);
                //UserManager.Update(_user);
                #endregion

                #region using stored proc
                var d1 = collection.Date_Created;
                var d2 = collection.Date_End;
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@0", userID));
                parameters.Add(new SqlParameter("@1", collection.Name));
                parameters.Add(new SqlParameter("@2", collection.Details));
                parameters.Add(new SqlParameter("@3", DateTime.Now));
                parameters.Add(new SqlParameter("@4", collection.Date_End));
                parameters.Add(new SqlParameter("@5", collection.PrizeFirst));
                parameters.Add(new SqlParameter("@6", collection.PrizeSecond));
                parameters.Add(new SqlParameter("@7", collection.PrizeThird));
                parameters.Add(new SqlParameter("@8", true));
                parameters.Add(new SqlParameter("@9", false));

                context.Database.ExecuteSqlCommand("exec spAddJob @0, @1, @2, @3, @4, @5, @6, @7, @8, @9", parameters.ToArray());
                #endregion


            }
            else //for update
            {
                Job dbEntry = context.Jobs.Find(job.Job_ID);
                if (dbEntry != null)
                {
                    dbEntry.Name = job.Name;
                    dbEntry.Details = job.Details;
                    //TODO: kulang pa
                }
            }
            context.SaveChanges();
        }

        public void PI_AddEarnings(int jobID, double amount)
        {
            var earningsNew = new Earnings
            {
                JobID = jobID,
                Date_Created = DateTime.Now,
                Amount = amount
            };
            context.Earnings.Add(earningsNew);
            context.SaveChanges();
        }

        public void SaveComment(Comments comment, string userID)
        {
            var commentNew = new Comments
            {
                commentDetails = comment.commentDetails,
           //     UserId = userID,
                //JobID = comment.JobID

            };
            context.Comments.Add(commentNew);
            context.SaveChanges();
        }

        public void editComment(Comments comment)
        {
            context.Entry(comment).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void SavePhoto(Photo photo, string userID)
        {

            var newPhoto = new Photo
            {
                PhotoId = photo.PhotoId,
                AcceptWorker = photo.AcceptWorker,
                CreatedOn = photo.CreatedOn,
                Description = photo.Description,
                ImagePath = photo.ImagePath,
                ThumbPath = photo.ThumbPath,


            };



            if (newPhoto.PhotoId == 0)
            {
                context.Photos.Add(newPhoto);



            }
            else
            {
                Photo dbEntry = context.Photos.Find(newPhoto.PhotoId);
                if (dbEntry != null)
                {
                    //dbEntry.Name = job.Name;
                    //dbEntry.Details = job.Details;
                    //TODO: kulang pa
                }
            }
            context.SaveChanges();
        }


        public void SaveFirst(int jobID, string userID)
        {
            var job = context.Jobs.Where(x => x.Job_ID == jobID).FirstOrDefault();

            if (job != null)
            {

            }

        }

        public void FirstWinner(int id, string userID)
        {
            var _job = context.Jobs.Where(x => x.Job_ID == id).FirstOrDefault();
            _job.WinnerFirst = userID;
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //add the prizes to the Winner
            var _user = manager.FindById(userID);
            _user.credits += _job.PrizeFirst;
            _user.totalWinnings += _job.PrizeFirst;
            manager.Update(_user);
            context.SaveChanges();

        }

        public void SecondWinner(int id, string userID)
        {
            var _job = context.Jobs.Where(x => x.Job_ID == id).FirstOrDefault();
            _job.WinnerSecond = userID;
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //add the prizes to the Winner
            var _user = manager.FindById(userID);
            _user.credits += _job.PrizeSecond;
            _user.totalWinnings += _job.PrizeSecond;
            manager.Update(_user);
            context.SaveChanges();
        }

        public void ThirdWinner(int id, string userID)
        {

            var _job = context.Jobs.Where(x => x.Job_ID == id).FirstOrDefault();
            _job.WinnerThird = userID;
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //add the prizes to the Winner
            var _user = manager.FindById(userID);
            _user.credits += _job.PrizeThird;
            _user.totalWinnings += _job.PrizeThird;
            manager.Update(_user);
            context.SaveChanges();
        }

        public double PI_TotalEarnings()
        {
            var total = context.Earnings.Sum(x => x.Amount);
            return total;
        }

        public void PI_DeductEarnings(int jobID, double amount)
        {
            throw new NotImplementedException();
        }
      

        public void CreateTestTable()
        {

            //  DataTable test = SQLHelper.ExecuteProc("spListOfTables", new object[] { }, ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            IEnumerable<string> results = context.Database.SqlQuery<string>("EXEC spListOfTables", null);
            // context.Database.ExecuteSqlCommand("Create table test (id int);");
            //var test = context.Database.SqlQuery<object>("spListOfTables").ToList();
            //var test1 = test.ToString();
            //using (context)
            //{
            //    context.Database.Connection.Open();
            //    var com = context.Database.Connection.CreateCommand();
            //    com.CommandText = "dbo.spListOfTables";
            //    com.CommandType = System.Data.CommandType.StoredProcedure;
            //    string test = com.ExecuteReader().ToString();
            //}


            // context.Database.SqlQuery<object>(
            //"exec InsertTest @p1 , @p2",
            //new SqlParameter("@p1", "a"),
            //new SqlParameter("@p2", 1)).FirstOrDefault();
        }

        public void AcceptChallenge(int jobId, string userId)
        {
            var job = context.Jobs.Where(j => j.Job_ID == jobId).FirstOrDefault();
            //-------------
            //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //ApplicationUser _user = UserManager.FindById(userId);
            //----------------

            //var myJob = _user.Jobs.Where(x => x.Job_ID == jobId).FirstOrDefault();
         
            if (job != null)
            {
                #region using ef
                //job.AcceptWorkers.Add(new AcceptWorker { UserId = userId });
                ////context.Entry
                ////context.AcceptWorkers.Add(new AcceptWorker { UserId = userId, JobId = jobId });

                //context.Database.Log = message => Trace.Write(message);
                //context.SaveChanges();
                ////myJob.AcceptWorkers.Add(new AcceptWorker { UserId = userId });

                ////UserManager.Update(_user);
                #endregion

                #region using stored proc
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@0", userId));
                parameters.Add(new SqlParameter("@1", false));
                parameters.Add(new SqlParameter("@2", jobId));
                parameters.Add(new SqlParameter("@3", false));


                context.Database.ExecuteSqlCommand("exec spAcceptChallenge  @0,@1,@2,@3",parameters.ToArray());
                #endregion



            }
        }

        public IEnumerable<ApplicationUser> GetTopWorkers()
        {
            var getTopWorker = (from getTop10 in context.Users
                                orderby getTop10.totalWinnings descending
                                where getTop10.totalWinnings > 0
                                select getTop10).Take(10);

            return getTopWorker;
        }

        public Job GetRecentClosedJob(string userId)
        {
            var jobs = (from recentClose in context.Jobs
             where recentClose.UserId == userId
             && recentClose.isOpen == false
             select recentClose).FirstOrDefault();

            return jobs;
        }

        public void AddCredit(string userId)
        {

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var currentUser = manager.FindById(userId);
            currentUser.credits += 1000;
            manager.Update(currentUser);

        }

        public IEnumerable<Job> AcceptedJobs(int jobId, string userId)
        {
            context.Database.Log = message => Trace.Write(message);
            var theJob = context.Jobs.Where(x => x.AcceptWorkers.Any(y => y.UserId == userId));

            return theJob;
   
        }

        public void SubmitEntry(int id,string userId, HttpPostedFileBase file)
        {

            //id ung job_ID
           context.Database.Log = message => Trace.Write(message);
            var _userId = userId;
           var workerPool = context.AcceptWorkers.Where(m => m.JobId == id && m.UserId == _userId).FirstOrDefault();


                byte[] data = GetBytesFromFile(file);

                var entry = new WorkerEntry
                {
                    Image = data,
                    UserId = _userId,
                    AcceptWorker = workerPool

                };

                workerPool.WorkerEntries.Add(entry);
                workerPool.IsSubmitted = true;

                context.SaveChanges();

            
        }

        public class SQLHelper
        {
            public static DataTable ExecuteProc(string procedureName, object[] parameterList, string SQLConnectionString) // throws SystemException
            {
                DataTable outputDataTable;

                using (SqlConnection sqlConnection = new SqlConnection(SQLConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand(procedureName, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        if (parameterList != null)
                        {
                            for (int i = 0; i < parameterList.Length; i = i + 2)
                            {
                                string parameterName = parameterList[i].ToString();
                                object parameterValue = parameterList[i + 1];

                                sqlCommand.Parameters.Add(new SqlParameter(parameterName, parameterValue));
                            }
                        }

                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        DataSet outputDataSet = new DataSet();
                        try
                        {
                            sqlDataAdapter.Fill(outputDataSet, "resultset");
                        }
                        catch (SystemException systemException)
                        {
                            // The source table is invalid.
                            throw systemException; // to be handled as appropriate by calling function
                        }

                        outputDataTable = outputDataSet.Tables["resultset"];
                    }
                }

                return outputDataTable;
            }
        }

        private byte[] GetBytesFromFile(HttpPostedFileBase file)
        {
            using (System.IO.Stream inputStream = file.InputStream)
            {
                System.IO.MemoryStream memoryStream = inputStream as System.IO.MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new System.IO.MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                return memoryStream.ToArray();
            }
        }
    }

    public enum DataType
    {
        INT, STRING, DATE
    }

    public class MyDataType
    {
        public string Value { get; set; }
        private MyDataType(string value) { Value = value; }

        public static MyDataType INT { get { return new MyDataType("INT"); } }
        public static MyDataType STRING { get { return new MyDataType("NVARCHAR(250)"); } }


        public static MyDataType DATE { get { return new MyDataType("DATETIME"); } }



    }
      

   
}

/* two ways to store sp
 * 
 *  var courseList = ctx.Database.SqlQuery<Course>(
            "exec GetCoursesByStudentId @StudentId ", idParam).ToList<Course>();
        

         var courseList = ctx.Courses.SqlQuery(
            "exec GetCoursesByStudentId @StudentId ", idParam).ToList<Course>();
*/