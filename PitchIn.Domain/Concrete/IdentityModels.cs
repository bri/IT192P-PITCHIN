﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PitchIn.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace PitchIn.Domain.Concrete
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            //Jobs = new List<Entities.Job>();
        }
        public bool isBanned { get; set; }
        public int credits { get; set; }

        public double Balance { get; set; }

        public decimal totalPayout { get; set; }

        public decimal totalWinnings { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual System.Collections.Generic.ICollection<File> Files { get; set; }

      //  public List<Job> Job { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }

        public virtual ICollection<Comments> Comments { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create() 
        {
            return new ApplicationDbContext();
        }

        public DbSet<Job> Jobs { get; set; }

        public DbSet<AcceptWorker> AcceptWorkers { get; set; }

        public DbSet<WorkerEntry> WorkerEntries { get; set; }

        public DbSet<File> Files { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<Comments> Comments { get; set; }

        public DbSet<Earnings> Earnings { get; set; }

        public DbSet<PI> PI { get; set; }

        public DbSet<Tables> Tables { get; set; }
    }

    public static class SecurityRoles
    {
        public const string Client = "client";
        public const string Worker = "worker";
    }
    public enum FileType
    {
        Avatar = 1, Photo
    }

    public class File
    {
        public int FileId { get; set; }
        [StringLength(255)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public FileType FileType { get; set; }
        public int PersonId { get; set; }
        public virtual ApplicationUser Person { get; set; }

        internal static void AppendAllText(string path, string data)
        {
            throw new NotImplementedException();
        }
    }

    public class Tables
    {
       
        public Int64 Id { get; set; }
        public string Table_Name { get; set; }
    }

}













/** 
* For the brave souls who get this far: You are the chosen ones, 
* the valiant knights of programming who toil away, without rest, 
* fixing our most awful code. To you, true saviors, kings of men, 
* I say this: never gonna give you up, never gonna let you down, 
* never gonna run around and desert you. Never gonna make you cry,
 * never gonna say goodbye. Never gonna tell a lie and hurt you. */

