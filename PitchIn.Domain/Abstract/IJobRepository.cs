﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PitchIn.Domain.Entities;
using PitchIn.Domain.Concrete;

namespace PitchIn.Domain.Abstract
{
  public  interface IJobRepository
    {
        IEnumerable<Job> Jobs
        {
            get;
        }


        void SaveJob(Job job,string userID);

        Job DeleteJob(int jobId);

        void SaveComment(Comments comment, string userID);

        void editComment(Comments comment);

        IEnumerable<Job> GetOpenJobs();

        IEnumerable<AcceptWorker> AcceptWorkers { get;}
      
        IEnumerable<WorkerEntry> WorkerEntries { get; }

        IEnumerable<Job> RecenttFiveJobsCreated();

        IEnumerable<Comments> Comments { get; }
        IEnumerable<Photo> Photos { get; }

        IEnumerable<Earnings> Earnings { get; }
        void SavePhoto(Photo photo, string userID);
        void FirstWinner(int id, string userID);
        void SecondWinner(int id, string userID);
        void ThirdWinner(int id, string userID);

        double PI_TotalEarnings();
        void PI_AddEarnings(int jobID, double amount);
        void PI_DeductEarnings(int jobID, double amount);

        void CreateTestTable();

        void AcceptChallenge(int jobId, string userId);

        void SubmitEntry(int id,string userId, System.Web.HttpPostedFileBase file);
        IEnumerable<Job> AcceptedJobs(int jobId, string userId);
        IEnumerable<ApplicationUser> GetTopWorkers();
         Job GetRecentClosedJob(string userId);
         void AddCredit(string userId);

    }
}
